package com.example.Librairie.Service;

import com.example.Librairie.Exception.LivreNotFoundException;
import com.example.Librairie.Modele.Livre;
import com.example.Librairie.Repository.LivreRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class LivreService {
    private final LivreRepository liv;
    public LivreService (LivreRepository l) {
        this.liv = l;
    }
    public Livre addLivre(Livre l) {return liv.save(l);}
    public List<Livre> findAllLivre() {
        return liv.findAll();
    }

    public Livre RechercheLivre(Long idLivre) {
        return liv.findById(idLivre).orElseThrow(() -> new LivreNotFoundException(
                "livre by id " + idLivre + " was not found"));
    }

    public void deleteLivre(Long idLivre){liv.deleteById(idLivre);
    }


}
