package com.example.Librairie.Controle;

import com.example.Librairie.Modele.Livre;
import com.example.Librairie.Repository.LivreRepository;
import com.example.Librairie.Service.LivreService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Livre")
@CrossOrigin(origins ="*")
public class LivreController {
    private final LivreRepository liv;

    private final LivreService service;

    public LivreController (LivreService service, LivreRepository liv) {
        super();
        this.service = service;
        this.liv = liv;

    }


    @GetMapping("/livres")
    public ResponseEntity<List<Livre>> getAllLivres() {
        List<Livre> livs = service.findAllLivre();
        return new ResponseEntity<>(livs, HttpStatus.OK);
    }
    @GetMapping("/find/{idLivre}")
    public ResponseEntity<Livre> getproduitByIdLivre (@PathVariable("idLivre") Long idLivre) {
        Livre liv = service.RechercheLivre(idLivre);
        return new ResponseEntity<>(liv, HttpStatus.OK);
    }



    @PostMapping("/add")
    public ResponseEntity<Livre> addEtudiant(@RequestBody Livre livre) {
        Livre newliv = service.addLivre(livre);
        return new ResponseEntity<>(newliv, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idLivre}")
    public ResponseEntity<?> deleteLivre(@PathVariable("idLivre") Long idLivre ) {
        service.deleteLivre(idLivre);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/update/{idLivre}")
    public ResponseEntity<Object> updatelivre(@RequestBody Livre l ,@PathVariable("idLivre") Long idLivre ) {
        Optional<Livre> livreOptional = liv.findById(idLivre);
        if (livreOptional.isEmpty())
            return ResponseEntity.notFound().build();
        l.setIdLivre(idLivre);
        liv.save(l);
        return ResponseEntity.noContent().build();
    }



}
