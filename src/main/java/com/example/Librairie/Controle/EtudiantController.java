package com.example.Librairie.Controle;

import com.example.Librairie.Modele.Etudiant;
import com.example.Librairie.Repository.EtudiantRepository;
import com.example.Librairie.Service.EtudiantService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Etudiant")
@CrossOrigin(origins ="*")
public class EtudiantController {
    private final EtudiantRepository etud;

    private final EtudiantService service;

    public EtudiantController (EtudiantService service, EtudiantRepository repo) {
        super();
        this.service = service;
        this.etud = repo;
    }

    @GetMapping("/etuds")
    public ResponseEntity<List<Etudiant>> getAllEtudiants() {
        List<Etudiant> prods = service.findAllEtudiants();
        return new ResponseEntity<>(prods, HttpStatus.OK);
    }
    @GetMapping("/find/{idEtudiant}")
    public ResponseEntity<Etudiant> getproduitByIdEtudiant (@PathVariable("idEtudiant") Long idEtudiant) {
        Etudiant etu = service.findEtudiantByIdEtudiant(idEtudiant);
        return new ResponseEntity<>(etu, HttpStatus.OK);
    }



    @PostMapping("/add")
    public ResponseEntity<Etudiant> addEtudiant(@RequestBody Etudiant etudia) {
        Etudiant newetud = service.addEtudiant(etudia);
        return new ResponseEntity<>(newetud, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idEtudiant}")
    public ResponseEntity<?> deleteEtudiant(@PathVariable("idEtudiant") Long idEtudiant ) {
        service.deleteEtudiant(idEtudiant);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/update/{idEtudiant}")
    public ResponseEntity<Object> updateEtudiant(@RequestBody Etudiant etudiant ,@PathVariable("idEtudiant") Long idEtudiant ) {
        Optional<Etudiant> ProduitOptional = etud.findById(idEtudiant);
        if (ProduitOptional.isEmpty())
            return ResponseEntity.notFound().build();
        etudiant.setIdEtudiant(idEtudiant);
        etud.save(etudiant);
        return ResponseEntity.noContent().build();
    }


}
