package com.example.Librairie.Exception;

public class EtudiantNotFoundException extends RuntimeException {
    public EtudiantNotFoundException(String message){super(message);}
}
