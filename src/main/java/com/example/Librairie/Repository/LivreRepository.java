package com.example.Librairie.Repository;

import com.example.Librairie.Modele.Livre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivreRepository extends JpaRepository<Livre, Long> {
       Livre findLivreByIdLivre(Long idLivre);
}
